Ion Programming Language
========================

Grammar
-------

```
COMMENT = "//" [^"\n" ^EOF]* ("\n" | EOF)
        | "/*" [^"*/"]* "*/"

DIGIT = [0-9]
HEXDIGIT = DIGIT | [a-fA-F]
LETTER = [a-zA-Z]

INT = DIGIT ("_" | DIGIT)*
    | "0" ("x"|"X") ("_"* HEXDIGIT "_"*)+
    | "0" ("b"|"B") ("_"* ("0"|"1") "_"*)+

NAME = LETTER (DIGIT | LETTER | "_")*
     | "_" (DIGIT | LETTER | "_")+

KEYWORD = "_" | "if" | "else" | "do" | "while" | "for" | "switch" | "case"
        | "break" | "continue" | "return" |
        | "var" | "const" | "func" | "struct" | "true" | "false"

ARITH_BINOP = "+" | "-" | "*" | "/" | "%"
LOGIC_BINOP = "&" | "|" | "^"
SHIFT_BINOP = "<<" | "<<<" | ">>" | ">>>"
CMP_BINOP = "==" | "!=" | "<" | "<=" | ">" | ">="
BINOP = ARITH_BINOP | LOGIC_BINOP | SHIFT_BINOP | CMP_BINOP
UNOP = "+" | "-" | "!" | "~"

primitive = INT | NAME
term = primitive
     | UNOP term
     | "(" expr ")"
expr = term (BINOP expr)?
start = expr? EOF
```


Comments
--------

Ion supports single-line comments and multi-line comments. Single-line comments start with `//` and
last until the end of the line or the end of the file is reached. Multi-line comments are enclosed
in `/*` and `*/`. Ion has no support for nested comments. A multi-line comment must be closed. If 
the end of the file is reached it will cause to a compile error.

```
// valid comment
/* valid multi-line comment */
/* valid
 * multi-line
 * comment
 */
/* valid /* multi-line comment */
/* INVALID /* mulit-line */ comment */
/* INVALID multi-line comment
```


Identifiers
-----------

Identifiers are used to name things in Ion. All alphanumerical characters and the underscore are
allowed to use. The first character must be either a letter or the underscore, but not a digit.
The identifier `_` is reserved and used by the compiler internally to mark values that are not
important and can be skipped. An underscore can thus not be used as an identifier as all other
reserved keywords.

```
someVar  // OK
var123   // OK
v__123   // OK
_var_    // OK
_        // ERROR
123var   // ERROR
```

The Ion naming convention envisages variable names, names of struct fields, function names to start
with a lower case and use camel-case or underscores as separator. Struct names start with an 
upper case.


Numerical Constants
-------------------

Integer literals in Ion support underscores as separators. Integers come in 3 flavors. Decimal
numbers start with a decimal digit, followed by any number of digits and underscores. There are
no negative numbers. Negative numbers are threated as an unary negation that is resolved during
compilation. Binary numbers start with `0b` or `0B` followed by any number of ones and zeros and
underscores. Hexadecimal numbers start with `0x` or `0X` and any number of hexadecimal digits and
underscores. Binary and hexadecimal numbers must contain at least one digit. Integer literals have
no type or length. They are converted to a type once they are assigned to a variable or if they
are combined with a variable within an expression.

```
0      // valid decimal number
123    // valid decimal number
0_     // valid decimal number
1___2  // valid decimal number
0b__0  // valid binary number
0B011  // valid binary number
0b___  // INVALID binary number
0xA_F  // valid hex number
0X     // INVALID hex number
```


Expressions
-----------

Ion supports the typical arithmetic operations `+`, `-`, `*`, `/` and `%`. The unary negation
operator `-` is unsed to flip a value's sign. The unary `+` operator is supported but does nothing.
Furthermore, they typical shift operators `<<`, `>>` and `>>>` are used to shift a value by some
number of bits. For bit manipulation `&`, `|` and `^` are used for AND, OR and XOR and `~` for NOT.
Expressions can be enclosed in parentheses. Expressions are resolved from left to right. Arithmetic
operators follow the associativity and distributivity rules.

Operators have the following precedence priorities:
* parentheses ()
* unary +, -, ~
* binary *, /, %
* binary +, -
* binary <<, >>, >>>
* binary &, ^
* binary |


Variables
---------

Variables bind a name to some memory location. A variable has an address of the memory it resides
in, a value stored at that memory location and the type of that value. Variables can change their
value during program execution. Variables are initialized to a zero-value, but one can force the
compiler to skip the initialization, for example if you are going to initialize it from the user's
input. Accessing an uninitialized variable will lead to a compile error. The type of a variable can
be inferred from the initial value.

```
var x : int = 2;  // full declaration with initial value
var y := 3;       // type int inferred
var z : int;      // initialized with 0
var r : int = _;  // _ stands for raw value
```


Functions
---------

Function group executable code under one name. Functions can have parameters and optionally one
return value. The return value can be a tuple, thus it is practically possible for a function to
return multiple values at once. Function blocks contain declarations and statements. It is thus
allowed to declare local functions and structs within a function, which cannot be returned to the
outside.


```
func factorial :: (n: int) -> int {
  // ...
}

func sort :: (a: int[]) -> {
  func sortRange :: (a: int[], start: int, end: int) -> {
    // ...
    sortRange(a, 0, middle-1);
    sortRange(a, middle, a.length-1);
  }

  sortRange(a, 0, a.length-1);
}

func toInt :: (s: string) -> (int, bool) {
  // ...
  if (noerrors) {
    return (value, true);
  } else {
    return (0, false);
  }
}

func f :: () -> {
  var value, ok := toInt("abc");
  var value, _  := toInt("123");  // bool is omitted
}
```


-----------------------------------------

Error cases:

```
TOKEN_NONE    -> should not happen!
TOKEN_ERROR   -> invalid symbol -> raise AST_ERROR
TOKEN_EOF     -> end of AST_EXPR
TOKEN_COMMENT -> ignore, parse again
TOKEN_INT     -> EXPR_INT
TOKEN_NAME    -> EXPR_NAME
TOKEN_KEYWORD -> raise AST_ERROR
TOKEN_SYMBOL  -> EXPR_<OP> or AST_ERROR

start(term)   = INT | NAME | UNOP | "("
start(expr)   = INT | NAME | UNOP | "("
start(start)  = INT | NAME | UNOP | "(" | EOF
follow(term)  = BINOP | EOF
follow(expr)  = ")" | EOF
follow(start) = EOF
error(term@2) = token!=start(term)
error(term@3) = token!=start(expr) | token!=")"
error(expr@1) = token!=start(expr)
error(start@1)= ERROR | token!=start(term) | KEYWORD
```
