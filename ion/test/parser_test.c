#include "cunit.h"
#include "util.h"

#include "parser.h"

#include "error.h"
#include "strintern.h"

#include <stdio.h>


GENERATE_ASSERT_EQUAL_ENUM(ASTKind)
GENERATE_ASSERT_EQUAL_ENUM(ExprKind)


#define assertASTNode(n, k) __assertASTNode(__FILE__, __LINE__, n, k)
static bool __assertASTNode(const char* file, int line, const ASTNode* node, ASTKind kind) {
  if (!__assertNotNull(file, line, node)) {
    return false;
  }
  if (!__assertEqualEnum(ASTKind, file, line, node->kind, kind)) {
    return false;
  }
  return true;
}


#define assertASTExpr(n, k) __assertASTExpr(__FILE__, __LINE__, n, k)
static bool __assertASTExpr(const char* file, int line, const ASTNode* node, ExprKind kind) {
  if (!__assertASTNode(file, line, node, AST_EXPR)) {
    return false;
  }
  if (!__assertEqualEnum(ExprKind, file, line, node->expr.kind, kind)) {
    return false;
  }
  return true;
}


static Number num(const char* n) {
  return numFromString(stringFromArray(n));
}


static ASTNode* createNode(ASTKind kind) {
  ASTNode* node = (ASTNode*) calloc(1, sizeof(ASTNode));
  node->kind = kind;
  return node;
}


static ASTNode* none() {
  return createNode(AST_NONE);
}

static ASTNode* error(ASTNode* fault, int n, ...) {
  ASTNode* node = createNode(AST_ERROR);
  node->faultyNode = fault;
  va_list args;
  va_start(args, n);
  for (int i = 0; i < n; i++) {
    string msg = va_arg(args, string);
    sbufPush(node->messages, msg);
  }
  va_end(args);
  return node;
}


static string errorMsg(const char* in, Location start, Location appearance, Location end,
                       const char* message) {
  Source src = sourceFromString(in);
  string msg = generateError(&src, start, appearance, end, message);
  deleteSource(&src);
  return msg;
}


static string noteMsg(const char* in, Location start, Location appearance, Location end,
                      const char* message) {
  Source src = sourceFromString(in);
  string msg = generateNote(&src, start, appearance, end, message);
  deleteSource(&src);
  return msg;
}


static ASTNode* exprName(const char* name) {
  ASTNode* node = createNode(AST_EXPR);
  node->expr.kind = EXPR_NAME;
  node->expr.name = stringFromArray(name);
  return node;
}


static ASTNode* exprInt(Number num) {
  ASTNode* node = createNode(AST_EXPR);
  node->expr.kind = EXPR_INT;
  node->expr.value = num;
  return node;
}


static ASTNode* exprUnop(const char* op, ASTNode* rhs) {
  ASTNode* node = createNode(AST_EXPR);
  node->expr.kind = EXPR_UNOP;
  node->expr.op = stringFromArray(op);
  node->expr.rhs = rhs;
  return node;
}


static ASTNode* exprBinop(const char* op, ASTNode* lhs, ASTNode* rhs) {
  ASTNode* node = createNode(AST_EXPR);
  node->expr.kind = EXPR_BINOP;
  node->expr.lhs = lhs;
  node->expr.op = stringFromArray(op);
  node->expr.rhs = rhs;
  return node;
}


static ASTNode* exprParen(ASTNode* expr) {
  ASTNode* node = createNode(AST_EXPR);
  node->expr.kind = EXPR_PAREN;
  node->expr.expr = expr;
  return node;
}


static TestResult testNode(const ASTNode* node, const ASTNode* exp) {
  TestResult result = {};

  ABORT(assertNotNull(node));
  ABORT(assertEqualEnum(ASTKind, node->kind, exp->kind));

  TEST(assertEqualInt(sbufLength(node->messages), sbufLength(exp->messages)));
  int min = sbufLength(node->messages) <= sbufLength(exp->messages) ?
            sbufLength(node->messages) : sbufLength(exp->messages);
  if (min > 0) {
    INFO("test messages");
    for (int i = 0; i < min; i++) {
//      printf("%s", node->messages[i].chars);
//      printf("%s", exp->messages[i].chars);
      TEST(assertEqualStr(node->messages[i], exp->messages[i].chars));
    }
  }

  switch (exp->kind) {
    case AST_NONE:
      break;

    case AST_ERROR:
      INFO("test faulty node");
      result = unite(result, testNode(node->faultyNode, exp->faultyNode));
      break;

    case AST_EXPR:
      ABORT(assertEqualEnum(ExprKind, node->expr.kind, exp->expr.kind));

      switch (exp->expr.kind) {
        case EXPR_NONE:
          break;
        case EXPR_INT:
          TEST(assertEqualNumber(node->expr.value, exp->expr.value));
          break;
        case EXPR_NAME:
          TEST(assertEqualStr(node->expr.name, exp->expr.name.chars));
          break;
        case EXPR_PAREN:
          INFO("test paren expr");
          result = unite(result, testNode(node->expr.expr, exp->expr.expr));
          break;
        case EXPR_UNOP:
          TEST(assertEqualStr(node->expr.op, exp->expr.op.chars));
          INFO("test unop rhs");
          result = unite(result, testNode(node->expr.rhs, exp->expr.rhs));
          break;
        case EXPR_BINOP:
          TEST(assertEqualStr(node->expr.op, exp->expr.op.chars));
          INFO("test binop lhs");
          result = unite(result, testNode(node->expr.lhs, exp->expr.lhs));
          INFO("test binop rhs");
          result = unite(result, testNode(node->expr.rhs, exp->expr.rhs));
          break;
      }

      break;
  }

  return result;
}


typedef struct TestCase {
  char*    name;
  char*    input;
  ASTNode* exp;
} TestCase;


static SBUF(TestCase) g_testCases = NULL;
static int            g_current = 0;


static TestResult testFunc() {
  TestResult result = {};
  TestCase* testCase = &g_testCases[g_current++];
  Source src = sourceFromString(testCase->input);
  ASTNode* node = parse(&src);
  result = testNode(node, testCase->exp);
  deleteNode(node);
  deleteSource(&src);
  return result;
}


static void createTest(TestSuite* suite, const char* input, ASTNode* exp) {
  TestCase testCase = { };
  testCase.name = (char*) malloc(100);
  snprintf(testCase.name, 100, "parse \"%s\"", input);
  testCase.input = unescape(input);
  testCase.exp = exp;
  sbufPush(g_testCases, testCase);
  __addTest(suite, &testFunc, testCase.name);
}


/********************************************* TESTS *********************************************/


static void addTestsEmptyString(TestSuite* suite) {
  const char* in;

  in = "";
  createTest(suite, in,
    none()
  );

  in = " ";
  createTest(suite, in,
    none()
  );

  in = "\\n";
  createTest(suite, in,
    none()
  );
}


static void addTestsExprName(TestSuite* suite) {
  const char* in;

  in = "x";
  createTest(suite, in,
    exprName("x")
  );

  in = "_1x2A";
  createTest(suite, in,
    exprName("_1x2A")
  );
}


static void addTestsExprInt(TestSuite* suite) {
  const char* in;

  in = "123";
  createTest(suite, in,
    exprInt(num("123"))
  );

  in = "0b_1111_0000";
  createTest(suite, in,
    exprInt(num("0b11110000"))
  );

  in = "0x_1234_ABCD";
  createTest(suite, in,
    exprInt(num("0x1234ABCD"))
  );
}


static void addTestsExprUnop(TestSuite* suite) {
  const char* in;

  in = "-x";
  createTest(suite, in,
    exprUnop("-", exprName("x"))
  );

  in = "+x";
  createTest(suite, in,
    exprUnop("+", exprName("x"))
  );

  in = "!x";
  createTest(suite, in,
    exprUnop("!", exprName("x"))
  );

  in = "~x";
  createTest(suite, in,
    exprUnop("~", exprName("x"))
  );

  in = "- x";
  createTest(suite, in,
    exprUnop("-", exprName("x"))
  );

  in = "+-x";
  createTest(suite, in,
    exprUnop("+", exprUnop("-", exprName("x")))
  );
}


static void addTestsExprBinop(TestSuite* suite) {
  const char* in;

  in = "x + y";
  createTest(suite, in,
    exprBinop("+", exprName("x"), exprName("y"))
  );

  in = "x - y";
  createTest(suite, in,
    exprBinop("-", exprName("x"), exprName("y"))
  );

  in = "x * 2";
  createTest(suite, in,
    exprBinop("*", exprName("x"), exprInt(num("2")))
  );

  in = "x / 2";
  createTest(suite, in,
    exprBinop("/", exprName("x"), exprInt(num("2")))
  );

  in = "x % 10";
  createTest(suite, in,
    exprBinop("%", exprName("x"), exprInt(num("10")))
  );

  in = "x+-y";
  createTest(suite, in,
    exprBinop("+", exprName("x"), exprUnop("-", exprName("y")))
  );

  in = "x++y";
  createTest(suite, in,
    exprBinop("+", exprName("x"), exprUnop("+", exprName("y")))
  );
}


static void addTestsExprBinopAssociativity(TestSuite* suite) {
  const char* in;

  in = "x + y + z";
  createTest(suite, in,
    exprBinop("+", exprBinop("+", exprName("x"), exprName("y")), exprName("z"))
  );

  in = "x + y - z";
  createTest(suite, in,
    exprBinop("-", exprBinop("+", exprName("x"), exprName("y")), exprName("z"))
  );

  in = "x * y * z";
  createTest(suite, in,
    exprBinop("*", exprBinop("*", exprName("x"), exprName("y")), exprName("z"))
  );

  in = "x * y / z";
  createTest(suite, in,
    exprBinop("/", exprBinop("*", exprName("x"), exprName("y")), exprName("z"))
  );

  in = "x * y % z";
  createTest(suite, in,
    exprBinop("%", exprBinop("*", exprName("x"), exprName("y")), exprName("z"))
  );
}


static void addTestsExprBinopDistributivity(TestSuite* suite) {
  const char* in;

  in = "x * y + z";
  createTest(suite, in,
    exprBinop("+", exprBinop("*", exprName("x"), exprName("y")), exprName("z"))
  );

  in = "x + y * z";
  createTest(suite, in,
    exprBinop("+", exprName("x"), exprBinop("*", exprName("y"), exprName("z")))
  );

  in = "x * y + z * w";
  createTest(suite, in,
    exprBinop("+", exprBinop("*", exprName("x"), exprName("y")),
                   exprBinop("*", exprName("z"), exprName("w"))
    )
  );

  in = "x + y * z + w";
  createTest(suite, in,
    exprBinop("+", exprBinop("+", exprName("x"), exprBinop("*", exprName("y"), exprName("z"))),
                   exprName("w")
    )
  );
}


static void addTestsComment(TestSuite* suite) {
  const char* in;

  in = "// comment";
  createTest(suite, in,
    none()
  );

  in = "x // comment";
  createTest(suite, in,
    exprName("x")
  );

  in = "x /**/ /**/";
  createTest(suite, in,
    exprName("x")
  );

  in = "-/*comment*/x";
  createTest(suite, in,
    exprUnop("-", exprName("x"))
  );

  in = "-/**/x";
  createTest(suite, in,
    exprUnop("-", exprName("x"))
  );

  in = "-/**//**/x";
  createTest(suite, in,
    exprUnop("-", exprName("x"))
  );

  in = "x-/**//**/y";
  createTest(suite, in,
    exprBinop("-", exprName("x"), exprName("y"))
  );

  in = "x + /*y*/ + z";
  createTest(suite, in,
    exprBinop("+", exprName("x"), exprUnop("+", exprName("z")))
  );
}


static void addTestsExprParen(TestSuite* suite) {
  const char* in;

  in = "(x)";
  createTest(suite, in,
    exprParen(exprName("x"))
  );

  in = "(x";
  createTest(suite, in,
    error(exprParen(exprName("x")), 2,
          errorMsg(in, loc(1, 1), loc(1, 3), loc(1, 3), "missing closing ')'"),
          noteMsg(in, loc(1, 1), loc(1, 1), loc(1, 1), "to match this '('")
    )
  );

  in = "((x)";
  createTest(suite, in,
    error(exprParen(exprParen(exprName("x"))), 2,
          errorMsg(in, loc(1, 1), loc(1, 5), loc(1, 5), "missing closing ')'"),
          noteMsg(in, loc(1, 1), loc(1, 1), loc(1, 1), "to match this '('")
    )
  );

  in = "x)";
  createTest(suite, in,
    error(exprName("x"), 1,
          errorMsg(in, loc(1, 2), loc(1, 2), loc(1, 2), "expected Token[TOKEN_EOF]")
    )
  );

  in = "((x";
  createTest(suite, in,
    error(exprParen(error(exprParen(exprName("x")), 2,
                          errorMsg(in, loc(1, 2), loc(1, 4), loc(1, 4), "missing closing ')'"),
                          noteMsg(in, loc(1, 2), loc(1, 2), loc(1, 2), "to match this '('")
                         )
                   ), 2,
          errorMsg(in, loc(1, 1), loc(1, 4), loc(1, 4), "missing closing ')'"),
          noteMsg(in, loc(1, 1), loc(1, 1), loc(1, 1), "to match this '('")
    )
  );

  in = "()";
  createTest(suite, in,
    error(exprParen(none()), 1,
          errorMsg(in, loc(1, 1), loc(1, 2), loc(1, 2), "missing expression")
    )
  );

  in = "(x + y)";
  createTest(suite, in,
    exprParen(exprBinop("+", exprName("x"), exprName("y")))
  );

  in = "x + (y)";
  createTest(suite, in,
    exprBinop("+", exprName("x"), exprParen(exprName("y")))
  );

  in = "x + (y - z)";
  createTest(suite, in,
    exprBinop("+", exprName("x"), exprParen(exprBinop("-", exprName("y"), exprName("z"))))
  );

  in = "(x + y) - z";
  createTest(suite, in,
    exprBinop("-", exprParen(exprBinop("+", exprName("x"), exprName("y"))), exprName("z"))
  );
}


static void addTestsError(TestSuite* suite) {
  const char* in;

  in = "$";
  createTest(suite, in,
    error(none(), 1,
          errorMsg(in, loc(1, 1), loc(1, 1), loc(1, 1), "illegal character '$'")
    )
  );

  in = "if";
  createTest(suite, in,
    error(none(), 1,
          errorMsg(in, loc(1, 1), loc(1, 1), loc(1, 2), "unexpected Token[TOKEN_KEYWORD if]")
    )
  );

  in = "_";
  createTest(suite, in,
    error(none(), 1,
          errorMsg(in, loc(1, 1), loc(1, 1), loc(1, 1), "unexpected Token[TOKEN_KEYWORD _]")
    )
  );

  in = "123x";
  createTest(suite, in,
    error(none(), 1,
          errorMsg(in, loc(1, 1), loc(1, 4), loc(1, 4), "invalid integer format")
    )
  );

  in = "/x";
  createTest(suite, in,
    error(none(), 1,
          errorMsg(in, loc(1, 1), loc(1, 1), loc(1, 1), "invalid unary operator /")
    )
  );

  in = "+";
  createTest(suite, in,
    error(exprUnop("+", error(none(), 1,
                              errorMsg(in, loc(1, 2), loc(1, 2), loc(1, 2),
                                       "unexpected Token[TOKEN_EOF]")
                        )
          ), 2,
          errorMsg(in, loc(1, 2), loc(1, 2), loc(1, 2), "missing operand"),
          noteMsg(in, loc(1, 1), loc(1, 1), loc(1, 1), "for unary operator +")
    )
  );

  in = "!if";
  createTest(suite, in,
    error(exprUnop("!", error(none(), 1,
                              errorMsg(in, loc(1, 2), loc(1, 2), loc(1, 3),
                                       "unexpected Token[TOKEN_KEYWORD if]")
                        )
          ), 2,
          errorMsg(in, loc(1, 2), loc(1, 2), loc(1, 3), "missing operand"),
          noteMsg(in, loc(1, 1), loc(1, 1), loc(1, 1), "for unary operator !")
    )
  );

  in = "!#x";
  createTest(suite, in,
    error(exprUnop("!", error(none(), 1,
                              errorMsg(in, loc(1, 2), loc(1, 2), loc(1, 2),
                                       "illegal character '#'")
                        )
          ), 2,
          errorMsg(in, loc(1, 2), loc(1, 2), loc(1, 2), "missing operand"),
          noteMsg(in, loc(1, 1), loc(1, 1), loc(1, 1), "for unary operator !")
    )
  );
}


TestResult parser_alltests(PrintLevel verbosity) {
  TestSuite suite = newSuite("TestSuite<parser>", "Test parser.");
  addTestsEmptyString(&suite);
  addTestsExprName(&suite);
  addTestsExprInt(&suite);
  addTestsExprUnop(&suite);
  addTestsExprBinop(&suite);
  addTestsExprBinopAssociativity(&suite);
  addTestsExprParen(&suite);
  addTestsComment(&suite);
  addTestsError(&suite);
  addTestsExprBinopDistributivity(&suite);

  TestResult result = run(&suite, verbosity);

  deleteSuite(&suite);
  for (int i = 0; i < sbufLength(g_testCases); i++) {
    free(g_testCases[i].name);
    free(g_testCases[i].input);
    deleteNode(g_testCases[i].exp);
  }
  sbufFree(g_testCases);
  g_current = 0;
  strinternFree();

  return result;
}
