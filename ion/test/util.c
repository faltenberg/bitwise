#include "util.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>


bool writeFile(const char* fileName, const char* content) {
  FILE* file = fopen(fileName, "w");
  if (file == NULL) {
    fclose(file);
    return false;
  }

  if (fputs(content, file) == EOF) {
    fclose(file);
    return false;
  }

  fclose(file);
  return true;
}


bool deleteFile(const char* fileName) {
  return remove(fileName) == 0;
}


char* unescape(const char* in) {
  char* out = (char*) malloc(strlen(in) + 1);

  bool esc = false;
  int i = 0;
  while (*in != '\0') {
    if (esc) {
      esc = false;

      switch (*in) {
        case '\\':  out[i++] = '\\';  break;
        case '\'':  out[i++] = '\'';  break;
        case '\"':  out[i++] = '\"';  break;
        case 'r':   out[i++] = '\r';  break;
        case 'n':   out[i++] = '\n';  break;
        case 't':   out[i++] = '\t';  break;
        case 'v':   out[i++] = '\v';  break;
        case '0':   out[i++] = '\0';  break;
        case 'a':   out[i++] = '\a';  break;
        case 'b':   out[i++] = '\b';  break;
        case 'e':   out[i++] = '\e';  break;
        case 'f':   out[i++] = '\f';  break;
      }
    } else if (*in == '\\') {
      esc = true;
    } else {
      out[i++] = *in;
    }

    in++;
  }

  out[i] = '\0';
  return out;
}


bool __assertEqualStr(const char* file, int line, string s, const char* cs) {
  printVerbose(__PROMPT, file, line);
  string exp = stringFromArray(cs);

  if (s.len != exp.len) {
    printVerbose(RED "ERROR: " RST);
    printVerbose("in \"%.*s\"%s expected length [%d] == [%d]\n",
                 (s.len <= 5 ? s.len : 5), s.chars, (s.len > 5 ? "~" : ""), s.len, exp.len);
    return false;
  }

  int index = 0;
  bool equal = true;
  while (index < s.len && equal) {
    equal = s.chars[index] == exp.chars[index];
    if (equal) {
      ++index;
    }
  }

  if (equal) {
    printVerbose(GRN "OK\n" RST);
    return true;
  } else {
    printVerbose(RED "ERROR: " RST);
    printVerbose("in \"%.*s\"%s[%d] expected [%c] == [%c]\n",
                 (s.len <= 5 ? s.len : 5), s.chars, (s.len > 5 ? "~" : ""), index,
                 s.chars[index], exp.chars[index]);
    return false;
  }
}


bool __assertEqualNumber(const char* file, int line, Number num, Number exp) {
  printVerbose(__PROMPT, file, line);

  if (num.value == exp.value) {
    printVerbose(GRN "OK" RST " (not implemented)\n");
    return true;
  } else {
    printVerbose(RED "ERROR: " RST);
    printVerbose("expected Number [%d] == [%d] (not implemented)\n",
                 num.value, exp.value);
    return false;
  }
}
