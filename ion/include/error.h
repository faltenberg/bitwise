#ifndef __ERROR_H__
#define __ERROR_H__


/**
 * Error
 * =====
 *
 * During the parsing process many errors may appear like illegal characters or missing closing
 * braces. This header provides methods to generate messages from source code. These messages
 * are allocated on the heap and must be freed!
 *
 *
 * Example
 * -------
 *
 * ```c {.line-numbers}
 * #include "error.h"
 * #include "source.h"
 * #include "str.h"
 * #include <assert.h>
 * #include <stdio.h>
 * #include <stdlib.h>
 *
 * int main() {
 *   Source src = sourceFromString("  12AB  ");
 *   // generate an error appearing at (1, 5) for "12AB" stretching from (1, 3) to (1, 6)
 *   string msg = generateError(&src, (1, 3), (1, 5), (1, 6),
 *                              "invalid character '%c' for an integer", src.content.chars[4]);
 *   // try also out generateWarning(), generateHint(), generateNote()
 *   assert(cstrequal(msg,
 *          "<cstring>:1:5: Error: invalid character 'A' for an integer\n  12AB  \n  ~~^~  \n"
 *   );
 *   printf("%.*s", msg.len, msg.chars);
 *   strFree(&msg);
 * }
 * ```
 */


#include "str.h"
#include "loc.h"
#include "source.h"


/**
 * `generateError()` generates an error message of this form:
 * ```txt
 * <file>:<line>:<pos>: Error: <message>
 * <line content from the file>
 *       ~~~^~~~
 * ```
 *
 * For the underlining the start and end location of the text fragment within the source must be
 * provided. Probably that will come directly from a token. The `^` will appear at the provided
 * caret location. The message will be generated from the format string and additional parameters
 * in a printf-fashion.
 *
 * - **param:** `src`    - the source file for the message
 * - **param:** `start`  - the start location of the underlined text fragment
 * - **param:** `caret`  - the location for the highlight
 * - **param:** `end`    - the end location of the underlined text fragment
 * - **param:** `format` - the format string for the message
 * - **param:** `...`    - the arguments specified by the format string
 * - **return:** the generated message
 */
string generateError(const Source* src, Location start, Location caret, Location end,
                     const char* format, ...);


/**
 * `generateNote()` generates a note message of this form:
 * ```txt
 * <file>:<line>:<pos>: Note: <message>
 * <line content from the file>
 *       ~~~^~~~
 * ```
 *
 * For the underlining the start and end location of the text fragment within the source must be
 * provided. Probably that will come directly from a token. The `^` will appear at the provided
 * caret location. The message will be generated from the format string and additional parameters
 * in a printf-fashion.
 *
 * - **param:** `src`    - the source file for the message
 * - **param:** `start`  - the start location of the underlined text fragment
 * - **param:** `caret`  - the location for the highlight
 * - **param:** `end`    - the end location of the underlined text fragment
 * - **param:** `format` - the format string for the message
 * - **param:** `...`    - the arguments specified by the format string
 * - **return:** the generated message
 */
string generateNote(const Source* src, Location start, Location caret, Location end,
                    const char* format, ...);


/**
 * `generateWarning()` generates a warning message of this form:
 * ```txt
 * <file>:<line>:<pos>: Warning: <message>
 * <line content from the file>
 *       ~~~^~~~
 * ```
 *
 * For the underlining the start and end location of the text fragment within the source must be
 * provided. Probably that will come directly from a token. The `^` will appear at the provided
 * caret location. The message will be generated from the format string and additional parameters
 * in a printf-fashion.
 *
 * - **param:** `src`    - the source file for the message
 * - **param:** `start`  - the start location of the underlined text fragment
 * - **param:** `caret`  - the location for the highlight
 * - **param:** `end`    - the end location of the underlined text fragment
 * - **param:** `format` - the format string for the message
 * - **param:** `...`    - the arguments specified by the format string
 * - **return:** the generated message
 */
string generateWarning(const Source* src, Location start, Location caret, Location end,
                       const char* format, ...);

/**
 * `generateHint()` generates a hint message of this form:
 * ```txt
 * <file>:<line>:<pos>: Hint: <message>
 * <line content from the file>
 *       ~~~^~~~
 * ```
 *
 * For the underlining the start and end location of the text fragment within the source must be
 * provided. Probably that will come directly from a token. The `^` will appear at the provided
 * caret location. The message will be generated from the format string and additional parameters
 * in a printf-fashion.
 *
 * - **param:** `src`    - the source file for the message
 * - **param:** `start`  - the start location of the underlined text fragment
 * - **param:** `caret`  - the location for the highlight
 * - **param:** `end`    - the end location of the underlined text fragment
 * - **param:** `format` - the format string for the message
 * - **param:** `...`    - the arguments specified by the format string
 * - **return:** the generated message
 */
string generateHint(const Source* src, Location start, Location caret, Location end,
                    const char* format, ...);


#endif  // __ERROR_H__
