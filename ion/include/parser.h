#ifndef __PARSER_H__
#define __PARSER_H__


/**
 * Parser
 * ======
 *
 * The `Parser` transforms a token stream to an abstract syntax tree for further introspection.
 * In order to catch as many (meaningful) errors as possible, the parser creates error nodes when
 * it detects some grammar violation. In any case, the parser always returns a valid node that must
 * be freed. In case of grammar violations the parser might not consume all the tokens. Let's say
 * the input is "-$2" and '$' will produce a TOKEN_ERROR then the parser cannot consume the '2' as
 * it has no rule for the invalid token. It can only return an error node and leave the remaining
 * token stream untouched. For information on the various AST node types, please refer to *ast.h*.
 *
 *
 * Example
 * -------
 *
 * ```c {.line-numbers}
 * #include "parser.h"
 * #include <assert.h>
 *
 * int main() {
 *   Source src = sourceFromString("(x + y) / 2x");
 *   ASTNode* node = parse(&src);
 *   assert(node->kind == AST_EXPR);
 *   assert(node->expr.kind == EXPR_BINOP);
 *   assert(cstrequal(node->expr.op, "/");
 *
 *   ASTNode* lhs = node->expr.lhs;
 *   assert(lhs->kind == AST_EXPR);
 *   assert(lhs->expr.kind == EXPR_PAREN);
 *
 *   lhs = lhs->expr.expr;
 *   assert(lhs->kind == AST_EXPR);
 *   assert(lhs->expr.kind == EXPR_BINOP);
 *   assert(cstrequal(lhs->expr.op, "+"));
 *
 *   ASTNode* rhs = node->expr.rhs;
 *   assert(rhs->kind == AST_ERROR);
 *   assert(sbufLength(rhs->messages) == 1);
 *   printf("%.*s", rhs.messages[0].len, rhs.messages[0].chars);
 *
 *   deletNode(node);
 *   deleteSource(&src);
 * }
 * ```
 */


#include "source.h"
#include "ast.h"


/**
 * `parse()` parses a source file to an abstract syntax tree.
 *
 * - **param:** `src` - the source code to be parsed
 * - **return:** the abstract syntax tree representing the source code
 */
ASTNode* parse(Source* src);


#endif  // __PARSER_H__
